﻿using ElevenNote.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ElevenNote.Services;

namespace ElevenNote.Web.Controllers
{
    [Authorize]
    public class NotesController : Controller
    {
        /// <summary>
        /// Instance of our Notes service.
        /// </summary>
        readonly NoteService _service = new NoteService();

        /// <summary>
        /// Gets the current logged in user's ID.
        /// </summary>
        Guid CurrentUserId
        {
            get
            {
                return new Guid(User.Identity.GetUserId());
            }
        }

        // GET: Notes
        public ActionResult Index(string sort = "")
        {
            if (TempData.ContainsKey("ErrorMessage"))
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            var notes = _service.GetAllForUser(this.CurrentUserId);
            //var sort = Request.QueryString["sort"] ?? ""; // via QueryString

            switch (sort)
            {
                case "name" :
                    notes = notes.OrderBy(o => o.Name).ToList();
                    break;
                case "modified" :
                    notes = notes.OrderBy(o => o.DateModified).ToList();
                    break;
                case "created" :
                    notes = notes.OrderBy(o => o.DateCreated).ToList();
                    break;
                default:
                    notes = notes.OrderBy(o => o.DateCreated).ToList();
                    break;
            }

            return View(notes);
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet()
        {
            var model = new NoteEditViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [ActionName("Create")]
        public ActionResult CreatePost(NoteEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                _service.Create(model, this.CurrentUserId);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult EditGet(int id = -1)
        {
            if (id == -1)
            {
                TempData.Add("ErrorMessage", "You didn't specify a note.");
                return RedirectToAction("Index");
            }

            // Attempt to get the note we're editing.
            var note = _service.GetById(id, this.CurrentUserId);

            // Make sure we got a note back.
            if (note == null)
            {
                TempData.Add("ErrorMessage", "That note couldn't be found.");
                return RedirectToAction("Index");
            }

            // If all looks good, pass the note to the view.
            return View(note);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(NoteEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                _service.Update(model, this.CurrentUserId);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var note = _service.GetById(id, this.CurrentUserId);
            if (note == null) return HttpNotFound();

            return View(note);
        }

        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int id)
        {
            // Attempt to get the note we're editing.
            var note = _service.GetById(id, this.CurrentUserId);

            // Make sure we got a note back.
            if (note == null) return HttpNotFound();

            // If all looks good, pass the note to the view.
            return View(note);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(int id)
        {
            if (ModelState.IsValid)
            {
                _service.Delete(id, this.CurrentUserId);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Toggle(int id = -1)
        {
            if (id == -1)
            {
                TempData.Add("ErrorMessage", "You didn't specify a note.");
                return RedirectToAction("Index");
            }

            // Attempt to get the note we're editing.
            var note = _service.GetById(id, this.CurrentUserId);

            // Make sure we got a note back.
            if (note == null)
            {
                TempData.Add("ErrorMessage", "That note couldn't be found.");
                return RedirectToAction("Index");
            }

            // If all looks good, toggle the IsFavorite.
            _service.ToggleFavorite(id, this.CurrentUserId);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Example of getting JSON data back with all our notes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetNotes()
        {
            var notes = _service.GetAllForUser(this.CurrentUserId);
            return Json(notes, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Web service to delete a note.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteNoteService(int id)
        {
            // Set the default result.
            var result = false;
            if (ModelState.IsValid && id > 0)
            {
                result = _service.Delete(id, this.CurrentUserId);
            }

            return Json(result);
        }

    }
}