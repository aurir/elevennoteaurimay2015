﻿using ElevenNote.Models.ViewModels;
using ElevenNote.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Web.Http;

namespace ElevenNote.WebServices.Controllers
{
    [Authorize]
    public class NotesController : ApiController
    {
        /// <summary>
        /// Example 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<NoteListViewModel> GetNotes()
        {
            var service = new NoteService();
            var userId = new Guid(User.Identity.GetUserId());
            return service.GetAllForUser(userId);
        }
    }
}